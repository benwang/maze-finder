import math
from heapq import heappush, heappop
import pygame
import cv2
import numpy as np
from pygame.constants import RESIZABLE

# Browse for maze image file
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo

img_filename = ""

def selectFile():
    global img_filename
    # create the root window
    root = tk.Tk()
    root.withdraw()

    img_filename = fd.askopenfilename(
        title='Select an image of a maze',
        initialdir='./Input')

    if len(img_filename) > 0:
        print(f"Using file {img_filename}")
    else:
        exit()


pygame.init()


# display_width = 800
# display_height = 600

# img_filename = 'Input/download.png'

CV_DEBUG = False

#####################################################
# Solvers
#####################################################

class Solver():
    name = "Unimplemented maze solver"
    
    START = 0 # Distance of 0
    UNVISITED = -1
    WALL = -2
    END = -4

    X = 0 # column
    Y = 1 # row

    def __init__(self, bitmap, start, end, xOffset, yOffset):
        self.start = [start[self.X] - xOffset, start[self.Y] - yOffset]
        self.end = [end[self.X] - xOffset, end[self.Y] - yOffset]
        self.xOffset = xOffset
        self.yOffSet = yOffset

        # Parse bitmask into map
        self.map = bitmap * (self.WALL - self.UNVISITED) + self.UNVISITED # Initialize unvisited cells to -1
        self.map[self.start[self.Y]][self.start[self.X]] = self.START
        self.map[self.end[self.Y]][self.end[self.X]] = self.END

        self.printMap()

        self.reachedEnd = False
        self.stepCount = 0

        # Track the longest path with this number
        self.totalNumPixels = 1

    def printMap(self):
        print("=========== Map ============")
        for row in self.map:
            rowStr = ""
            for col in row:
                rowStr += str(col)
            print(rowStr)
        print("=========== End ============\n")

    def draw(self, surface):
        pass

    def isFinished(self):
        pass

    def getStepCount(self):
        return self.stepCount
    
    def step(self):
        pass

    def calcDist(self, p1, p2):
        return math.sqrt(pow(p2[self.X] - p1[self.X], 2) + pow(p2[self.Y] - p1[self.Y], 2))

#####################################################

class Djikstra(Solver):
    name = "Djikstra"
    
    def __init__(self, bitmap, start, end, xOffset, yOffset):
        super().__init__(bitmap, start, end, xOffset, yOffset)

        self.queue = [[0, self.start[self.X], self.start[self.Y]]]

        # Make map for backtracking
        self.pathMap = []
        for _ in range(self.map.shape[0]):
            row = []
            for _ in range(self.map.shape[1]):
                row.append(None)
            self.pathMap.append(row)

        # How many steps (pixels) per frame we take
        self.steps_per_frame = int(self.map.shape[0] * self.map.shape[1] / 100) # 1% of area per frame
        
    def draw(self, surface):
        for r,rData in enumerate(self.map):
            for c,cData in enumerate(rData):
                if cData == self.START:
                    pygame.draw.circle(surface, (0, 100, 0), (c + self.xOffset, r + self.yOffSet), 2, 2)
                elif cData == self.END:
                    pygame.draw.circle(surface, (100, 0, 0), (c + self.xOffset, r + self.yOffSet), 2, 2)

                if cData > self.START:
                    surface.set_at((c + self.xOffset, r + self.yOffSet), (0, 0, int(255 * cData / self.totalNumPixels)))
        
        if self.isFinished():
            # Draw finished line
            currPoint = self.end
            while currPoint is not None and currPoint != self.start:
                surface.set_at((currPoint[self.X] + self.xOffset, currPoint[self.Y] + self.yOffSet), (255, 0, 0))
                currPoint = self.pathMap[currPoint[self.Y]][currPoint[self.X]]


    def isFinished(self):
        return len(self.queue) == 0 or self.reachedEnd
    
    def step(self):
        if self.isFinished():
            return
        
        for _ in range(self.steps_per_frame):
            if len(self.queue) > 0:
                self.stepCount += 1

                parentDist,x,y = heappop(self.queue)
                curr = [x,y]
                # parentDist = self.map[curr[self.Y]][curr[self.X]]

                # Keep tracking the longest path
                self.totalNumPixels = max(self.totalNumPixels, parentDist + math.sqrt(2))

                for dX in [-1,0,1]:
                    for dY in [-1,0,1]:
                        if dX == 0 and dY == 0:
                            continue

                        x = curr[self.X] + dX
                        y = curr[self.Y] + dY
                        currDist = parentDist + math.sqrt(abs(dX) + abs(dY))

                        if x < 0 or y < 0 or x >= self.map.shape[1] or y >= self.map.shape[0]:
                            continue

                        # Check for base case
                        if self.map[y][x] == self.END:
                            self.pathMap[y][x] = curr
                            self.reachedEnd = True
                            return

                        # Check if we found a better solution to a certain point. Requeue if so
                        if self.map[y][x] > self.START and self.map[y][x] > currDist:
                            print(f"Found better solution at [{y}][{x}] from {self.map[y][x]} to {currDist}")
                        elif self.map[y][x] != self.UNVISITED:
                            # Only process unvisited nodes
                            continue
                    
                        # Add to queue
                        heappush(self.queue, [currDist, x, y])
                        # self.queue.append([x, y])

                        # Update distance to node
                        self.map[y][x] = currDist
                        self.pathMap[y][x] = curr

#####################################################

class AStar(Solver):
    name = "A*"
    
    def __init__(self, bitmap, start, end, xOffset, yOffset):
        super().__init__(bitmap, start, end, xOffset, yOffset)

        self.queue = [[0, self.start[self.X], self.start[self.Y]]]

        # Make map for backtracking
        self.pathMap = []
        for _ in range(self.map.shape[0]):
            row = []
            for _ in range(self.map.shape[1]):
                row.append(None)
            self.pathMap.append(row)

        # How many steps (pixels) per frame we take
        self.steps_per_frame = int(self.map.shape[0] * self.map.shape[1] / 100) # 1% of area per frame
        
    def draw(self, surface):
        for r,rData in enumerate(self.map):
            for c,cData in enumerate(rData):
                if cData == self.START:
                    pygame.draw.circle(surface, (0, 100, 0), (c + self.xOffset, r + self.yOffSet), 2, 2)
                elif cData == self.END:
                    pygame.draw.circle(surface, (100, 0, 0), (c + self.xOffset, r + self.yOffSet), 2, 2)

                if cData > self.START:
                    surface.set_at((c + self.xOffset, r + self.yOffSet), (0, 0, int(255 * cData / self.totalNumPixels)))
        
        if self.isFinished():
            # Draw finished line
            currPoint = self.end
            while currPoint is not None and currPoint != self.start:
                surface.set_at((currPoint[self.X] + self.xOffset, currPoint[self.Y] + self.yOffSet), (255, 0, 0))
                currPoint = self.pathMap[currPoint[self.Y]][currPoint[self.X]]


    def isFinished(self):
        return len(self.queue) == 0 or self.reachedEnd
    
    def step(self):
        if self.isFinished():
            return
        
        for _ in range(self.steps_per_frame):
            if len(self.queue) > 0:
                self.stepCount += 1

                _,x,y = heappop(self.queue)
                curr = [x,y]
                parentDist = self.map[curr[self.Y]][curr[self.X]]

                # Keep tracking the longest path
                self.totalNumPixels = max(self.totalNumPixels, parentDist + math.sqrt(2))

                for dX in [-1,0,1]:
                    for dY in [-1,0,1]:
                        if dX == 0 and dY == 0:
                            continue

                        x = curr[self.X] + dX
                        y = curr[self.Y] + dY
                        currDist = parentDist + math.sqrt(abs(dX) + abs(dY))

                        if x < 0 or y < 0 or x >= self.map.shape[1] or y >= self.map.shape[0]:
                            continue

                        # Check for base case
                        if self.map[y][x] == self.END:
                            self.pathMap[y][x] = curr
                            self.reachedEnd = True
                            return

                        # Check if we found a better solution to a certain point. Requeue if so
                        if self.map[y][x] > self.START and self.map[y][x] > currDist:
                            print(f"Found better solution at [{y}][{x}] from {self.map[y][x]} to {currDist}")
                        elif self.map[y][x] != self.UNVISITED:
                            # Only process unvisited nodes
                            continue
                    
                        # Add to queue
                        heappush(self.queue, [currDist + self.calcDist([x, y], self.end), x, y])
                        # self.queue.append([x, y])

                        # Update distance to node
                        self.map[y][x] = currDist
                        self.pathMap[y][x] = curr

#####################################################
# UI elements
#####################################################

# UI text definition

# create a font object.
# 1st parameter is the font file
# which is present in pygame.
# 2nd parameter is size of the font
UI_ELEMENT_PADDING = 2 # px above and below text
FONT_SIZE = 14
font = pygame.font.Font('freesansbold.ttf', FONT_SIZE)

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Dimensions of UI
UI_TOP_HEIGHT = FONT_SIZE + UI_ELEMENT_PADDING * 2
BUTTON_HEIGHT = FONT_SIZE + UI_ELEMENT_PADDING * 4
UI_BOT_HEIGHT = BUTTON_HEIGHT

class Button():
    def __init__(self, x, y, id, text, callback, width, height):
        self.x = x
        self.y = y
        self.id = id
        self.text = text
        self.callback = callback
        self.width = width
        self.height = height
    
    def draw(self, surface):
        # create a text surface object,
        # on which text is drawn on it.
        # text = font.render(f'Steps: {solver.getStepCount()} Done: {solver.isFinished()} len: {solver.totalNumPixels}', True, BLACK)
        text = font.render(self.text, True, WHITE)
        
        # create a rectangular object for the
        # text surface object
        textRect = text.get_rect()
        
        # set the center of the rectangular object.
        textRect.left = self.x
        textRect.top = self.y
        textRect.width = self.width
        textRect.height = self.height
        # textRect.center = (display_width / 2, FONT_SIZE / 2 + UI_ELEMENT_PADDING)

        pygame.draw.rect(surface, (100,100,100), textRect)

        surface.blit(text, textRect)
    
    # Returns true if coord is within the boundaries of this button
    def handleClick(self, coord):
        clicked = coord[Solver.X] >= self.x and coord[Solver.X] <= self.x + self.width and coord[Solver.Y] >= self.y and coord[Solver.Y] <= self.y + self.height
        if clicked:
            self.callback()
        
        return clicked

#####################################################
# OpenCV portion
#####################################################

def processImage():
    global trimmedShapeMask, image, minRow, maxRow, minCol, maxCol, shapeMask
    # Identify walls
    image = cv2.imread(img_filename)
    if image is None:
        print("Failed to read image! Perhaps unsupported type?")
        exit()

    UPPER_THRESH = 220
    lower = np.array([0, 0, 0])
    upper = np.array([UPPER_THRESH, UPPER_THRESH, UPPER_THRESH])
    shapeMask = cv2.inRange(image, lower, upper)

    if CV_DEBUG:
        cv2.imshow("shapeMask", shapeMask)
        cv2.waitKey(0)

    # Trim off boundary
    def firstNonZero(x):
        for i,n in enumerate(x):
            if n > 0:
                return i
        raise Exception(f"No non-zero element found in {x}")

    def listMinMax(x):
        return [firstNonZero(x), len(x) - firstNonZero(reversed(x))]

    # Find rows to trim
    rowSum = np.sum(shapeMask, axis = 1).tolist()
    minRow,maxRow = listMinMax(rowSum)
    assert minRow < maxRow, "Image is blank!"

    # Find columns to trim
    colSum = np.sum(shapeMask, axis = 0).tolist()
    minCol,maxCol = listMinMax(colSum)
    assert minCol < maxCol, "Image is blank!"

    trimmedShapeMask = shapeMask[minRow:maxRow, minCol:maxCol]

    if CV_DEBUG:
        cv2.imshow("trimmed maze", trimmedShapeMask)
        cv2.waitKey(0)

#####################################################
# Pygame portion
#####################################################

def initUI():
    global display_width, display_height, image_top, image_bottom, gameDisplay, drawDisplay, mazeImg, buttons, bitmap, solverButton
    # Initialize UI
    display_width = image.shape[1]
    image_top = UI_TOP_HEIGHT
    image_bottom = image_top + image.shape[0]
    display_height = image_bottom + UI_BOT_HEIGHT

    gameDisplay = pygame.display.set_mode((display_width,display_height), RESIZABLE)
    drawDisplay = gameDisplay.copy()
    pygame.display.set_caption('Maze Finder')

    def cvimage_to_pygame(image):
        """Convert cvimage into a pygame image"""
        return pygame.image.frombuffer(image.tostring(), image.shape[1::-1], "RGB")

    mazeImg = cvimage_to_pygame(image)

    # Initialize solver
    bitmap = (trimmedShapeMask > 0) * 1
    print("Traversable bitmap:")
    print(bitmap)

    NUM_BUTTONS = 3
    buttonWidth = display_width / (NUM_BUTTONS * 2) - 2 * UI_ELEMENT_PADDING
    solverButton = Button(
        UI_ELEMENT_PADDING + (buttonWidth + 2 * UI_ELEMENT_PADDING) * 1,
        image_bottom + UI_ELEMENT_PADDING,
        "SWITCH_SOLVER",
        f"Solver: {SOLVERS[solverIndex].name}",
        toggleSolver,
        buttonWidth * 3 + 4 * UI_ELEMENT_PADDING, # backfill the missing UI padding
        BUTTON_HEIGHT
    )
    buttons = [
        Button(
            UI_ELEMENT_PADDING + (buttonWidth + 2 * UI_ELEMENT_PADDING) * 0,
            image_bottom + UI_ELEMENT_PADDING,
            "RESET",
            "Reset",
            resetMaze,
            buttonWidth * 1,
            BUTTON_HEIGHT
        ),
        solverButton,
        Button(
            UI_ELEMENT_PADDING + (buttonWidth + 2 * UI_ELEMENT_PADDING) * 4,
            image_bottom + UI_ELEMENT_PADDING,
            "LOAD",
            "Load Image",
            loadImage,
            buttonWidth * 2 + 2 * UI_ELEMENT_PADDING,
            BUTTON_HEIGHT
        ),
    ]

clock = pygame.time.Clock()
crashed = False

def drawMaze(surface, x,y):
    surface.blit(mazeImg, (x,y))

def drawUI(surface):
    # create a text surface object,
    # on which text is drawn on it.
    # text = font.render(f'Steps: {solver.getStepCount()} Done: {solver.isFinished()} len: {solver.totalNumPixels}', True, BLACK)
    statusString = f'Steps: {solver.getStepCount()} len: {solver.totalNumPixels:.2f}'
    if solver.isFinished():
        statusString += " Done!"
    text = font.render(statusString, True, WHITE)
    
    # create a rectangular object for the
    # text surface object
    textRect = text.get_rect()
    
    # set the center of the rectangular object.
    textRect.center = (display_width / 2, FONT_SIZE / 2 + UI_ELEMENT_PADDING)


    surface.blit(text, textRect)

def drawPoint(surface, coord, color):
    pygame.draw.circle(surface, color, coord, 5, 2)

def scaleDisplaySize(coord, displaySize, targetSize):
    return (int(coord[0] / displaySize[0] * targetSize[0]), int(coord[1] / displaySize[1] * targetSize[1]))

# Callback for reset button
def resetMaze():
    global startPoint, endPoint, solver
    print("Resetting maze")
    startPoint = None
    endPoint = None
    solver = None

solverIndex = 0
SOLVERS = [
    Djikstra,
    AStar
]
def toggleSolver():
    global solverIndex
    solverIndex = (solverIndex + 1) % len(SOLVERS)
    solverButton.text = f"Solver: {SOLVERS[solverIndex].name}"
    print(f'updated solver index to {solverIndex}')

def loadImage():
    selectFile()
    processImage()
    resetMaze()
    initUI()

# Initial load
loadImage()

while not crashed:
    # Process inputs
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True
        
        # Get clicks for start and end pos
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_presses = pygame.mouse.get_pressed()
            if mouse_presses[0]:
                # Get start and end points with click
                scaledClickPoint = scaleDisplaySize(pygame.mouse.get_pos(), gameDisplay.get_rect().size, drawDisplay.get_rect().size)

                # Start and end point selection
                if (
                    scaledClickPoint[Solver.Y] >= image_top + minRow and
                    scaledClickPoint[Solver.Y] < image_top + maxRow and
                    scaledClickPoint[Solver.X] >= minCol and
                    scaledClickPoint[Solver.X] < maxCol
                ):
                    if startPoint == None and shapeMask[scaledClickPoint[Solver.Y] - UI_TOP_HEIGHT][scaledClickPoint[Solver.X]] == 0:
                        startPoint = scaledClickPoint
                    elif endPoint == None and shapeMask[scaledClickPoint[Solver.Y] - UI_TOP_HEIGHT][scaledClickPoint[Solver.X]] == 0:
                        endPoint = scaledClickPoint
                        solver = SOLVERS[solverIndex](bitmap, startPoint, endPoint, minCol, minRow + UI_TOP_HEIGHT)
                        print("Initialized solver")
                    else:
                        print("Left Mouse key was clicked: ", pygame.mouse.get_pos())
                else:
                    # Check for button presses
                    for button in buttons:
                        if button.handleClick(scaledClickPoint):
                            print(f"Pressed button with ID '{button.id}'")


    # Run solver
    if solver and not solver.isFinished():
        solver.step()

    # Drawing section
    drawDisplay.fill(BLACK)
    drawMaze(drawDisplay, x=0.0, y=UI_TOP_HEIGHT)
    if solver:
        solver.draw(drawDisplay)
        drawUI(drawDisplay)
    if startPoint:
        drawPoint(drawDisplay, startPoint, (0,255,0))
    if endPoint:
        drawPoint(drawDisplay, endPoint, (255,0,0))
    
    for button in buttons:
        button.draw(drawDisplay)

    # Scale drawn display to window size
    gameDisplay.blit(pygame.transform.scale(drawDisplay, gameDisplay.get_rect().size), (0, 0))
    pygame.display.update()
    clock.tick(60)

pygame.quit()
quit()