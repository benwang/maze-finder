# Maze Finder

Python demo of various pathfinding algorithms.

## Setup

On Windows, run `setup.ps1` to install virtualenv, set up the venv, and install required python packages. On Linux, run `setup.sh`.

Rerun the script at any time to install updated packages from `requirements.txt`

## Activating the virtual requirement

### Windows

```bat
.\venv\Scripts\activate
```

### Linux

```bash
source venv/bin/activate
```

## Running

1. Download an image of a maze (preferably < 300px x 300px or it'll overwhelm the current pixel-based implementation) and place it in the `./Input/` folder.
2. Run `maze_finder.py`
3. It'll prompt you to select the image of the maze.
4. Once loaded (you may wish to resize the screen), left-click to select the start point. Left click again at the end point
5. Enjoy!

## Exporting packages

To update `requirements.txt`, run `pip freeze > requirements.txt`.